var express = require('express')
var bodyParser = require('body-parser');

var app = express();
app.use(express.static('public'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', require('./routes/index.js'))

// Have a look at route/sample.js for an example of route with get, post and tagged URL
//app.use('/sample',require('./route/sample'))

app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
