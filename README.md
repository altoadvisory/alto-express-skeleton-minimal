alto-express-skelton

The bare minimum to run an Express server with VSCode debugger configured.

Unzip to the folder of your choice and run 'npm i' from that folder.

Running the server: 'node.exe app.js'
Browse 'http://localhost:3000', you should get a 'Hello, world!'
Same if you browse 'http://localhost/hello.html'